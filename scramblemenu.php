<?php
include_once('newhtml.html');
?>

<html>
    <head>
        <title>Menu</title>
    </head>
    <script>
            if(!sessionStorage.getItem("login")){
                console.log("hello in redirect");
                location.replace('./scramblelogin.php')
               
            }

            // if(sessionStorage.getItem("login")){
            //     console.log("hello in redirect");
            //     location.replace('./scramblemenu.html')
               
            // }
    </script>
    <style>
        .parent{
           
            background-color: #0a3035;
            height: 100%;
            width:100%;
        }
        .child{
            
            height: 400px;
            width: 400px;
            background-image: radial-gradient(ellipse at left bottom, rgba(76, 126, 218, 0.692) 0%, rgba(44, 180, 221, 0.26) 60%,rgba(49, 192, 202, 0.925) 100%);
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            position: absolute;
            /* box-shadow: 5px 5px 5px 5px rgba(248, 32, 32, 0.2); */
            border-radius: 3%;
        }
        .button {
            color: #ffffff;
            font-size: 1rem;
            font-family: 'Montserrat', sans-serif;
            text-transform: uppercase;
            letter-spacing: 1px;
            margin-top: 1.5rem;
            padding: .75rem;
            border-radius: 2rem;
            display: block;
            width: 80%;
            background-color: rgba(18, 196, 196, 0.301);
            border: none;
            cursor: pointer;
            height: 70px;
            margin-left: 2.5rem;
        }

         .button:hover {
                    background-color: #0a3035;
                }
    </style>

    <body>
        <div class="parent">
            <div class="child">
               <div>
                <button type="button" class="button" value="BEGINNER" onclick="begin()">BEGINNER</button>
               </div>
               <div>
                <button type="button" class="button" value="INTERMEDIATE" onclick="intermediate()">INTERMEDIATE</button>
               </div>
               <div>
                <button type="button" class="button" value="EXPERT" onclick="expert()">EXPERT</button>
               </div>
               <div>
                <button type="button" class="button" value="LOG OUT" onclick="setData()">LOG OUT</button>
               </div>
            </div>
        </div>

    </body>
    <script>

        function begin(){

            window.location.replace("scramble.php");
        }

        

        function intermediate(){
            window.location.replace("scramble1.php");
        }

        function expert(){
            window.location.replace("scramble2.php");
        }

        function setData(){

                let a = sessionStorage.getItem('blevel');
                let b = sessionStorage.getItem('ilevel');
                let c = sessionStorage.getItem('elevel');
                let d = sessionStorage.getItem('username');
                console.log("in set in log ",a,b,c,d);


                let xmlHttp = new XMLHttpRequest();
                xmlHttp.open("POST", "setscramblelevel.php");

                xmlHttp.onreadystatechange = function(){
                    if(xmlHttp.readyState == 4){
                        sessionStorage.clear();
                        window.location.replace("scramblelogin.php");      
                    }
                }


                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlHttp.send("blevel="+a+"&ilevel="+b+"&elevel="+c+"&username="+d);
                
         }

    </script>
</html>