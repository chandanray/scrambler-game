<?php
include_once('newhtml.html');
?>
 <html>
    <head>
        <title> Log-in</title>
        <style>
            html, body{
                min-height: 100%;
                }

                body{
                background: #0a3035;
                font-family: 'Montserrat', sans-serif;
                font-size: 16px;
                line-height: 1.25;
                letter-spacing: 1px;
                }

                * {
                    box-sizing: border-box;
                    transition: .25s all ease;
                }

                .container{
                display: block;
                position: relative;
                z-index: 0;
                margin: 4rem auto 0;
                padding: 5rem 4rem 0 4rem;
                width: 100%;
                max-width: 525px;
                min-height: 680px;
                box-shadow: 0px 50px 70px -20px rgb(10, 0, 0,0.8);
                border-radius: 20px;
                }

                .container:after{
                content: '';
                display: inline-block;
                position: absolute;
                z-index: 0;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                background-image: radial-gradient(ellipse at left bottom, rgba(19, 94, 233, 0.692) 0%, rgba(44, 180, 221, 0.26) 60%,rgba(49, 192, 202, 0.925) 100%);
                box-shadow: 0 -20px 150px -20px rgba(0, 0, 0, 0.5);
                border-radius: 20px;
                }

                .form-app{
                position: relative;
                z-index: 1;
                padding-bottom: 4.5rem;
                border-bottom: 1px solid rgba(255,255,255, 0.25);
                }

                .nav{
                position: relative;
                padding: 0;
                margin: 0 0 6em 1rem;
                }

                .nav-item{
                list-style: none;
                display: inline-block;
                }

                .nav-item + .nav-item{
                margin-left: 2.25rem;
                }

                .nav-item a{
                position: relative;
                color: rgba(255, 255, 255, 0.5);
                text-decoration: none;
                text-transform: uppercase;
                font-weight: 500;
                font-size: 1.25rem;
                padding-bottom: .5rem;
                transition: .20s all ease;
                }

                .nav-item.active a, .nav-item a:hover{
                color: #FFFFFF;
                transition: .15s all ease;
                }

                .nav-item a:after{
                content: '';
                display: inline-block;
                height: 10px;
                background-color: rgb(255, 255, 255);
                position: absolute;
                right: 100%;
                bottom: -1px;
                left: 0;
                border-radius: 50%;
                transition: .15s all ease;
                }

                .nav-item a:hover:after, .nav-item.active a:after{
                background-color: rgb(208, 183, 181);
                height: 2px;
                right: 0;
                bottom: 2px;
                border-radius: 0;
                transition: .20s all ease;
                }

                .form-label, .form-checkbox-label{
                color: rgba(255, 255, 255, 0.5);
                text-transform: uppercase;
                font-size: .75rem;
                margin-bottom: 1rem;
                }

                .form-checkbox-label{
                display: inline-block;
                position: relative;
                padding-left: 1.5rem;
                margin-top: 2rem;
                margin-left: 1rem;
                color: #FFFFFF;
                font-size: .75rem;
                text-transform: inherit;
                }

                .form-input-text{
                color: white;
                font-size: 1.15rem;
                width: 100%;
                padding: .5rem 1rem;
                border: 2px solid transparent;
                outline: none;
                border-radius: 1.5rem;
                background-color: rgba(255, 255, 255, 0.25);
                letter-spacing: 1px;
                }

                .form-input-text:hover,
                .form-input-text:focus{
                    color: white;
                    border: 2px solid rgba(255, 255, 255, 0.5);
                    background-color: transparent;
                }

                .form-input-text + .form-label {
                    margin-top: 1.5rem;
                }

                .form-input-checkbox {
                    position: absolute;
                    top: .1rem;
                    left: 0;
                    margin: 0;
                }

                .login-button {
                    color: #ffffff;
                    font-size: 1rem;
                    font-family: 'Montserrat', sans-serif;
                    text-transform: uppercase;
                    letter-spacing: 1px;
                    margin-top: 1rem;
                    padding: .75rem;
                    border-radius: 2rem;
                    display: block;
                    width: 100%;
                    background-color: rgba(18, 196, 196, 0.301);
                    border: none;
                    cursor: pointer;
                }

                .login-button:hover {
                    background-color: #0a3035;
                }

                .link {
                    display: block;
                    margin-top: 3rem;
                    text-align: center;
                    color: rgba(255, 255, 255, 0.75);
                    font-size: .75rem;
                    text-decoration: none;
                    position: relative;
                    z-index: 1;
                }

                .link:hover {
                    color: rgb(235, 53, 53);
                }

        </style>

    </head>
    <body>
        <div class="container" id="1" >
                <form  class="form-app" onclick="valid1()">
                <ul class="nav">
                    <li class="nav-item active"><a href="#" >Sign in</a></li>
                    <li class="nav-item"><a href="#" onclick="signup();">Sign Up</a></li>
                </ul>
            
                <label for="login-user" class="form-label">Username</label>
                <input id="login-user" class="form-input-text" type="text" />
                <p> </p>
            
                <label for="login-pass" class="form-label">Password</label>
                <input id="login-pass" class="form-input-text" type="password" />
            
                <label for="login-checkbox" class="form-checkbox-label">
                        <input id="login-checkbox" class="form-input-checkbox" type="checkbox"> Keep me logged in
                </label>
            
                <button type="button" class="login-button" onclick="getData()">Sign in</button>
                <div id="message"></div>
                </form>
                
            
            <a href="#" class="link">Forgot Password?</a>
          </div>

          <div class="container" id="2" style="display:none">
              <form   class="form-app" >
                    <ul class="nav">
                            <li class="nav-item "><a href="#" onclick="signin();">Sign in</a></li>
                            <li class="nav-item active"><a href="#">Sign Up</a></li>
                    </ul>

                    <label for="login-user" class="form-label">Name</label>
                    <input id="name" name="name" class="form-input-text" type="text" />
                    <p> </p>


                    <label for="login-user" class="form-label">Last Name</label>
                    <input id="lastname" name="lastname" class="form-input-text" type="text" />
                    <p> </p>

                    <label for="login-user" class="form-label">Username</label>
                    <input id="username" name="username" class="form-input-text" type="text" onclick="valid1()" />
                    <p id="text"> </p>

                    <label for="login-pass" class="form-label">Password</label>
                    <input id="password" name="password" class="form-input-text" type="password" />

                    <button type="button" class="login-button" onclick=" setData();">Sign Up</button>

              </form>
          </div>
    </body>

    <script>
        function signin(){
           
            document.getElementById("2").style.display = "none";
            document.getElementById("1").style.display = "block";
           

        }

         function signup(){
           
            document.getElementById("1").style.display = "none";
            document.getElementById("2").style.display = "block";
           
        }

        function setData(){

           let name = document.getElementById("name").value;
           let lastname = document.getElementById("lastname").value;
           let username = document.getElementById("username").value;
           let password = document.getElementById("password").value;

            let xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "setscrambledata.php");
            xmlHttp.onreadystatechange = function(){
                if(xmlHttp.readyState == 4){
                    let result = JSON.parse(xmlHttp.responseText);
                    
                    if(result == "false"){
                        document.getElementById("2").style.display= "block";
                        document.getElementById("text").innerHTML = "Username already Exists!";
                    }
                    else{
                        document.getElementById("text").innerHTML = "";
                       
                    }
                    if(result == "true"){
                        document.getElementById("name").value = "";
                        document.getElementById("lastname").value = "";
                        document.getElementById("username").value = "";
                        document.getElementById("password").value = "";
                        document.getElementById("2").style.display = "none";
                        document.getElementById("1").style.display = "block";
                        console.log("hello");
                    }
                       
                                  
                   
                        
                        
                       
                }
            }

           
            xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlHttp.send("name="+name+"&lastname="+lastname+"&username="+username+"&password="+password);
          
        }

        function getData(){

           
           let username = document.getElementById("login-user").value;
           let password = document.getElementById("login-pass").value;

            let xmlHttp = new XMLHttpRequest();
            xmlHttp.open("POST", "getscrambledata.php");
            xmlHttp.onreadystatechange = function(){
                if(xmlHttp.readyState == 4){
                    console.log(xmlHttp.responseText);
                   let result = JSON.parse(xmlHttp.responseText);
                  
                   if(result.error ==  "true"){
                       console.log("in ",result.user.blevel);
                       sessionStorage.setItem("blevel", result.user.blevel);
                       sessionStorage.setItem("ilevel",result.user.ilevel);
                       sessionStorage.setItem("elevel", result.user.elevel);  
                       sessionStorage.setItem('username',result.user.username) ; 
                       sessionStorage.setItem("login", "true") ;                                                                   
                       sessionStorage.setItem("type", result.user.type);
                       window.location.replace("scramblemenu.php");
                   }else{
                       document.getElementById("message").innerHTML = "Please Enter Correct Username and Password";
                    
                   }

                }
            }
            xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlHttp.send("username="+username+"&password="+password);
        }

        function valid1(){
            document.getElementById("message").innerHTML = "";
            document.getElementById("text").innerHTML = "";
        }
    </script>
</html>

