<?php
include_once('newhtml.html');
?>
<html>
    <head>
        <title> Scramble </title>
        <link rel="icon" href="Bb.png"/>
    </head>
    <script>
            if(!sessionStorage.getItem("login")){
                console.log("hello in redirect");
                location.replace('./scramblelogin.php')
               
            }

            // if(sessionStorage.getItem("login")){
            //     console.log("hello in redirect");
            //     location.replace('./scramble2.html')
               
            // }
    </script>
    <style>
        * {
            box-sizing: border-box;
        }
        body{
            opacity: 1;
        }
       .parent{
           width: 100%;
           height: 100%;
           background-color: #2f4159;
          
           
       }
       .child{
           width:900px;
           height:550px;
           top: 0;
           bottom: 0;
           left: 0;
           right: 0;
           margin: auto;
           border-radius: 10px;
           background-image: radial-gradient(ellipse at left bottom, rgba(48, 116, 241, 0.692) 0%, rgba(44, 180, 221, 0.26) 60%,rgba(49, 192, 202, 0.925) 100%);
           padding:20px;
           position: absolute;
           box-shadow: -50px 50px 50px -50px rgb(8,0,0,.5);
       }
       td{
           background-color:  #0fbb99;
       }
       .popup {
            position: fixed;
            display: block;
            cursor: pointer;
            height: 200px;
            width: 400px;
            background-color:  #0a3035;
            bottom: 40%;
            right: 40%;
            border-radius: 5px;
            box-shadow: 2px 2px 2px 2px rgb(8,0,0,.5);
            opacity: 1;
           
            

        }
        .button {
            color: #ffffff;
            font-size: 1rem;
            font-family: 'Montserrat', sans-serif;
            text-transform: uppercase;
            letter-spacing: 1px;
            margin-top: 1.5rem;
            padding: .75rem;
            border-radius: 2rem;
            display: block;
            width: 80%;
            background-color: rgba(13, 205, 219, 0.952);
            border: none;
            cursor: pointer;
            height: 65px;
            margin-left: 2.5rem;
        }
        .overlay {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: rgba(0.2, 0.1, 0.1, 0.5);
            transition: opacity 500ms;
        
            opacity: 1;
            display: none;
        }
        .block{
            height:100px;
            width:100px;
            display:block;
            float:right;
            top:0;
            right:0;
            text-align: center;
            background-image: radial-gradient(ellipse at left bottom, rgba(48, 116, 241, 0.692) 0%, rgba(44, 180, 221, 0.26) 60%,rgba(49, 192, 202, 0.925) 100%);
            border-radius: 5px;
            color: aliceblue;
            font-family: 'Montserrat', sans-serif;
            cursor: pointer;
            z-index:10;
            position:fixed;
        }
        .blockb:hover{
            color:white;
            background-color: #0fbb99;
            box-shadow: 5px 5px 5px 5px rgb(8,0,0,.5);

        }


      
    </style>
    <body onload="word1()">
        <div class="parent">
            <div class="child">
                
              
                <style>
                    td{
                        font-size: 80px;
                        text-align: center;
                        height: 50%;
                        width:16%;
                    }
                </style>
                <table style="height:100%;width:100%;">
                    <tbody>
                        
                        <tr>
                            <td id="1" style="background-color:#62e5cb">
                                   
                            </td>
                            <td id="2" style="background-color:#62e5cb"> 
                                    
                            </td>
                            <td id="3" style="background-color:#62e5cb"> 
                                   
                            </td>
                            <td id="4" style="background-color:#62e5cb"> 
                                    
                            </td>
                            <td id="5" style="background-color:#62e5cb">
                                   
                            </td>
                            <td id="6" style="background-color:#62e5cb">
                                   
                            </td>

                        </tr>
                        <tr>
                                <td id="7" draggable="true"> 
                                        
                                </td>
                                <td  id="8" draggable="true"> 
                                      
                                </td>
                                <td id="9" draggable="true"> 
                                      
                                </td>
                                <td id="10" draggable="true"> 
                                      
                                </td>
                                <td id="11" draggable="true"> 
                                      
                                </td>
                                <td id="12" draggable="true"> 
                                      
                                </td>

                        </tr>
                    </tbody>
                </table>
               
            </div>
            <div class="block">
                    <div class="blockb" style="width:100%;height:50%;padding:15px;" onclick="quit()">
                        <div >
                                Quit
                        </div>
                    </div>
                    <div   style="width:100%;height:50%">
                        <div style="width:45%;height:100%;display: inline-block;">
                            Level-
                        </div>
                        <p style="width:45%;height:100%;display: inline-block;" id="score">
                              &nbsp;0
                        </p>
        
                    </div>
        
            </div>
            
        </div>
        <div class="overlay" id="overlay">            
             <div class="popup" id="popup" >
                    <button type="button" class="button" onclick="next1()">Next</button>
                    <button type="button" class="button" onclick="menu()">Main Menu</button>
                
            </div>
        </div>

            
    </body>
    <script>
        let val;
        let arr;
        let j ;
        let tdword = new Array(6);
            document.addEventListener("dragstart", function(event) {
                
                if(event.target.id == "7"){
                        val = document.getElementById("7").innerHTML;
                    
                }
                else 
                if(event.target.id == "8"){
                        val = document.getElementById("8").innerHTML;
                    
                }
                else
                if(event.target.id == "9"){
                        val = document.getElementById("9").innerHTML;
                }
                else
                if(event.target.id == "10"){
                        val = document.getElementById("10").innerHTML;
                }
                else
                if(event.target.id == "11"){
                        val = document.getElementById("11").innerHTML;
                }
                else
                if(event.target.id == "12"){
                        val = document.getElementById("12").innerHTML;
                }
            
            });
            
            document.addEventListener("dragover", function(event) {
                
                event.preventDefault();
            });

            document.addEventListener("drop", function(event) {
                  
                event.preventDefault();
                
                if(event.target.id == "1"){
                        document.getElementById("1").innerHTML = val;
                        let v = document.getElementById("1");
                        v.style.backgroundColor = "#0fbb99";
                        myFunction();
                    
                }else 
                if(event.target.id == "2"){
                        document.getElementById("2").innerHTML = val;
                        let v = document.getElementById("2");
                        v.style.backgroundColor = "#0fbb99";
                        myFunction();
                        
                }else
                if(event.target.id == "3"){
                        document.getElementById("3").innerHTML = val;
                        let v = document.getElementById("3");
                        v.style.backgroundColor = "#0fbb99";
                        myFunction();
                }else
                if(event.target.id == "4"){
                    document.getElementById("4").innerHTML = val ;
                    let v = document.getElementById("4");
                    v.style.backgroundColor = "#0fbb99";
                    myFunction();
                }
                else
                if(event.target.id == "5"){
                    document.getElementById("5").innerHTML = val ;
                    let v = document.getElementById("5");
                    v.style.backgroundColor = "#0fbb99";
                    myFunction();
                }
                else
                if(event.target.id == "6"){
                    document.getElementById("6").innerHTML = val ;
                    let v = document.getElementById("6");
                    v.style.backgroundColor = "#0fbb99";
                    myFunction();
                }
                else{
                    console.log("in last");
                }

            });

           
            function word1() {

                let xmlHttp = new XMLHttpRequest();
                xmlHttp.open("POST", "wordmatcher2.php");

                xmlHttp.onreadystatechange = function(){
                    if(xmlHttp.readyState == 4){

                        arr = JSON.parse(xmlHttp.responseText);
                        document.getElementById("1").innerHTML = "";
                        document.getElementById("2").innerHTML = "";
                        document.getElementById("3").innerHTML = "";
                        document.getElementById("4").innerHTML = "";
                        document.getElementById("5").innerHTML = "";
                        document.getElementById("6").innerHTML = "";

                        sessionStorage.getItem('elevel');
                        j=sessionStorage.getItem('elevel');
                        document.getElementById("score").innerHTML= j;
                        loadData(j);
                    }
                }
                xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlHttp.send();
                
            } 

        function loadData(i){
           
            let arr1 = arr[i].split("");
           
            arr1 = arr1.sort();
           
            document.getElementById("7").innerHTML = arr1[0];
            document.getElementById("8").innerHTML = arr1[1];
            document.getElementById("9").innerHTML = arr1[2];
            document.getElementById("10").innerHTML = arr1[3];
            document.getElementById("11").innerHTML = arr1[4];
            document.getElementById("12").innerHTML = arr1[5];
                      
        }

        function getField(){
            if(document.getElementById("1").innerHTML != "" && document.getElementById("2").innerHTML != "" && document.getElementById("3").innerHTML != "" && document.getElementById("4").innerHTML != "" && document.getElementById("5").innerHTML != "" && document.getElementById("6").innerHTML != "" ){
           
                tdword[0] = document.getElementById("1").innerHTML ;
                tdword[1] = document.getElementById("2").innerHTML ;
                tdword[2] = document.getElementById("3").innerHTML ;
                tdword[3] = document.getElementById("4").innerHTML ;
                tdword[4] = document.getElementById("5").innerHTML ;
                tdword[5] = document.getElementById("6").innerHTML ;
                    
                    let match = tdword.join("");
                    
                    if(arr[j] == match){
                        j++ ;
                        document.getElementById("score").innerHTML= j;
                        sessionStorage.setItem("elevel", j);
                        if(j<25){
                           loadData(j);
                        }
                        else{
                            document.getElementById("1").innerHTML = "";
                            document.getElementById("2").innerHTML = "";
                            document.getElementById("3").innerHTML = "";
                            document.getElementById("4").innerHTML = "";
                            document.getElementById("5").innerHTML = "";
                            document.getElementById("6").innerHTML = "";
                            document.getElementById("7").innerHTML = "";
                            document.getElementById("8").innerHTML = "";
                            document.getElementById("9").innerHTML = "";
                            document.getElementById("10").innerHTML = "";
                            document.getElementById("11").innerHTML = "";
                            document.getElementById("12").innerHTML = "";
                            window.location.replace("scramblemenu.php");
                        }
                        document.getElementById("1").innerHTML = "";
                        document.getElementById("2").innerHTML = "";
                        document.getElementById("3").innerHTML = "";
                        document.getElementById("4").innerHTML = "";
                        document.getElementById("5").innerHTML = "";
                        document.getElementById("6").innerHTML = "";
                        
                        document.getElementById("1").style.backgroundColor = "#62e5cb";
                        document.getElementById("2").style.backgroundColor = "#62e5cb";
                        document.getElementById("3").style.backgroundColor = "#62e5cb";
                        document.getElementById("4").style.backgroundColor = "#62e5cb";
                        document.getElementById("5").style.backgroundColor = "#62e5cb";
                        document.getElementById("6").style.backgroundColor = "#62e5cb";
                        
                        
                        
                    }
                    else{
                        document.getElementById("1").style.backgroundColor = "red";
                        document.getElementById("2").style.backgroundColor = "red";
                        document.getElementById("3").style.backgroundColor = "red";
                        document.getElementById("4").style.backgroundColor = "red";
                        document.getElementById("5").style.backgroundColor = "red";
                        document.getElementById("6").style.backgroundColor = "red";
                       console.log("no");
                    }
            
            }else{
                console.log("no outer if")
            }

           

        }   
        function myFunction() {
            if(document.getElementById("1").innerHTML != "" && document.getElementById("2").innerHTML != "" && document.getElementById("3").innerHTML != "" && document.getElementById("4").innerHTML != "" && document.getElementById("5").innerHTML != "" && document.getElementById("6").innerHTML != "" ){
                    tdword[0] = document.getElementById("1").innerHTML ;
                    tdword[1] = document.getElementById("2").innerHTML ;
                    tdword[2] = document.getElementById("3").innerHTML ;
                    tdword[3] = document.getElementById("4").innerHTML ;
                    tdword[4] = document.getElementById("5").innerHTML ;
                    tdword[5] = document.getElementById("6").innerHTML ;
                    
                    let match = tdword.join("");
                    
                    if(arr[j] == match){
                        document.getElementById("overlay").style.display = "block";
                        body.style.opacity = "0.3";
                    }
                    else{
                        document.getElementById("1").style.backgroundColor = "red";
                        document.getElementById("2").style.backgroundColor = "red";
                        document.getElementById("3").style.backgroundColor = "red";
                        document.getElementById("4").style.backgroundColor = "red";
                        document.getElementById("5").style.backgroundColor = "red";
                        document.getElementById("6").style.backgroundColor = "red";
                       console.log("no");
                    }
            }
        }

        function next1(){
            document.getElementById("overlay").style.display = "none";
            getField()
        }
        
       function menu(){
        window.location.replace("scramblemenu.php");
       }

        function quit(){
           window.location.replace("scramblemenu.php");
       }
                

    </script>
</html>